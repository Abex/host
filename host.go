package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	usage := `Usage:
$ host
	hosts http on port 8080
$ host :[port]
	hosts http on [port].
$ host [certfile] [keyfile]
	hosts https on port 8443
$ host :[port] [certfile] [keyfile]
	hosts https on [port]
$ host :[httpport] :[httpsport] [certfile] [keyfile]
	hosts https on [httpsport] and http on [httpport]`
	var phttp string
	var phttps string
	switch len(os.Args) {
	case 1:
		phttp = ":8080"
	case 2:
		phttp = os.Args[1]
		if phttp == "-h" || phttp == "--help" || phttp == "help" || phttp == "/?" {
			fmt.Println(usage)
			os.Exit(1)
		}
	case 3:
		phttps = ":443"
	case 4:
		phttps = os.Args[1]
	case 5:
		phttp = os.Args[1]
		phttps = os.Args[2]
	default:
		fmt.Println(usage)
		os.Exit(1)
	}
	mux := http.FileServer(http.Dir(""))
	if phttp != "" {
		go func() {
			panic(http.ListenAndServe(phttp, mux))
		}()
	}
	if phttps != "" {
		go func() {
			panic(http.ListenAndServeTLS(phttps, os.Args[len(os.Args)-2], os.Args[len(os.Args)-1], mux))
		}()
	}
	<-make(chan struct{})
}
